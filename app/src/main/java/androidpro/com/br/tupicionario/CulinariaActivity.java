package androidpro.com.br.tupicionario;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class CulinariaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_culinaria);

        final String[] culinaria_array = getResources().getStringArray(R.array.culinaria);
        final String[] culinaria_desc = getResources().getStringArray(R.array.culinaria_desc);

        ArrayList<Item> list = new ArrayList<>();

        for (int i=0; i<10; i++){

            String titulo = culinaria_array[i];
            String desc = culinaria_desc[i];

            Item item = new Item(titulo, desc, R.drawable.ic_culinaria);
            list.add(item);
        }

        ItemAdapter adapter = new ItemAdapter(this, list, R.color.culinaria_categoria);
        ListView listView = (ListView) findViewById(R.id.rootCulinaria);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                String desc = culinaria_desc[position];
                Toast.makeText(CulinariaActivity.this, desc, Toast.LENGTH_LONG).show();

            }
        });
        listView.setAdapter(adapter);

    }
}
