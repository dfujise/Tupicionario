package androidpro.com.br.tupicionario;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import static androidpro.com.br.tupicionario.R.array.bichos;

/**
 * Created by dan on 05/03/17.
 */

public class BichosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bichos);
        String[] bichos_array = getResources().getStringArray(bichos);
        final String[] bichos_descArray = getResources().getStringArray(R.array.bichos_desc);
        ArrayList<Item> list = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            String titulo = bichos_array[i];
            String desc = bichos_descArray[i];


            Item item = new Item(titulo, desc, R.drawable.ic_bichos);
            list.add(item);
        }

        ItemAdapter adapter = new ItemAdapter(this, list, R.color.bichos_categoria);
        ListView listView = (ListView) findViewById(R.id.rootBichos);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String desc = bichos_descArray[position];
                Toast.makeText(BichosActivity.this, desc, Toast.LENGTH_LONG).show();
            }
        });
        listView.setAdapter(adapter);
    }
}
